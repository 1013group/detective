﻿using System;
using System.Threading;

namespace SherlokHolms2004
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            Began(game);
        }
        static void Began(Game game)
        {
            Console.WriteLine("================================SherlokHolms=================================\n1.Начать игру\n2.Выйти из игры");
            try
            {
                switch(Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Console.Clear();
                        game.Play();
                        break;
                    case 2:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        Began(game);
                        break;
                }
            }
            catch
            {
                Console.WriteLine("Введите число! (1 или 2)");
                Thread.Sleep(2000);
                Console.Clear();
                Began(game);
            }
        }
    }
}
