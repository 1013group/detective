﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SherlokHolms2004
{
    internal class Game
    {
        Player player = new Player();
        CrimeSite site = new CrimeSite();
        string Crime = "Неизвестно";
        string[] hints = new string[3] { "Неизвестно", "", "" };
        bool Win = false;
        bool GameStart = true;


        public void Play() //80% игры
        {
            player.miniBiography();
            Next();

            Random random = new Random();
            int criminal = random.Next(site.participants.Length - 1);
            int tr = 0;

            while (GameStart)
            {
                Console.WriteLine("ЗДЕСЬ МОГЛА БЫТЬ ВАША РЕКЛАМА");
                Console.WriteLine("Преступление: " + Crime);
                Console.WriteLine("Место преступление: " + site.crimethemes[site.crimetheme]);
                Console.WriteLine("Приметы преступника:" + hints[0] + " " + hints[1] + " " + hints[2]);
                Console.WriteLine("Вы: " + player.Name);
                Console.WriteLine("1.Осмотреть место преступления\n2.Опросить свидетеля\n3.Выбрать преступника\n4.Опросить подозреваемых");
                //Информация
                try
                {
                    switch (Convert.ToInt32(Console.ReadLine())) //Выбирается вариант игрока
                    {
                        case 1://Осмотреть место преступления
                            if (site.crimetype == 0) { Console.WriteLine("Это убийство..."); Crime = site.crimetypes[1];}
                            else { Console.WriteLine("Это ограбление..."); Crime = site.crimetypes[0]; }
                            hints[0] = site.participants[criminal].hab[0];
                            Next();
                            break;
                        case 2://Опросить свидетеля
                            if (Crime == "Неизвестно") { Console.WriteLine("Я не буду с вами разговаривать, вы некомпетентны"); Next(); }
                            else
                            {
                                if (tr < 3)
                                {
                                    tr++;
                                    Console.WriteLine("Введётся диалог со свидетелем");
                                    Thread.Sleep(1000);
                                    int lucky = random.Next(101);
                                    if (lucky >= 60) { Console.WriteLine("Вы узнали новую примерную примету преступника"); hints[1] = site.participants[criminal].hab[1]; tr = 3; }
                                    else Console.WriteLine("Вы ничего нового не узнали");
                                }
                                else { Console.WriteLine("Свидетель: Я ничего больше не знаю"); }
                                Next();
                            }
                            break;
                        case 3://Выбор преступника
                            Console.WriteLine("Известные приметы:" + hints[0] + " " + hints[1] + " " + hints[2]);
                            for (int i = 0; i < site.participants.Length - 1; i++)
                                Console.WriteLine(site.participants[i].Get_Persone());
                            Console.Write("Введите номер преступника:");
                            ChoiceCriminal(criminal, Convert.ToInt32(Console.ReadLine()));
                            break;
                        case 4://Опросить подозреваемых
                            Console.Clear();
                            for (int i = 0; i < site.participants.Length - 1; i++)
                            {
                                Console.WriteLine(i + " " + site.participants[i].Get_Persone());
                                Console.WriteLine("==========================");
                            }
                            Next();
                            Talk(criminal);
                            break;
                        default://если написали число, но такого нету
                            Console.WriteLine("Выберите вариант ответа!");
                            Thread.Sleep(1000);
                            Next();
                            break;
                    }
                }
                catch//если какая-то ошибка
                {
                    Console.WriteLine("Введите число без пробелов");
                }
                Console.Clear();
            }
            if (Win) Console.WriteLine("Вы вычислили преступника, вас повысили в должности.");//конец игры
            Console.WriteLine("КОНЕЦ!");
            Console.WriteLine("Здесь тоже могла быть ваша реклама!!!!!                (Jocker Face) => :(:");
        }
        private void Talk(int criminal)//Допрос подозреваемых
        {
            Console.Clear();
            Console.WriteLine("Ведётся допрос... Каким детективом вы будете?\n1.Хорошим\n2.Плохим");
            try
            {
                Random rand = new Random();
                int lucky = rand.Next(101);
                int choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 2)
                {
                    if (lucky >= 80)
                    {
                        Console.WriteLine("Вам повезло и преступник сдался");
                        Win = true;
                    }
                    else
                        Console.WriteLine("Вам не удалось узнать что-то и вы умерли от инфаркта...");
                    GameStart = false;
                }
                else if (choice == 1)
                {
                    if (lucky >= 50)
                    {
                        Console.WriteLine("Вы узнали новую примету преступника");
                        hints[2] = site.participants[criminal].hab[2];
                    }
                    else Console.WriteLine("Вам не удалось узнать что-то новое");
                }
                else Console.WriteLine("Введите число 1 или 2");
            }
            catch { Console.WriteLine("Введите число без пробелов"); }
            Next();
        }
        private void ChoiceCriminal(int criminal, int choice)//Выбор преступника
        {
            if (choice > -1 && choice < 3)
            {
                if (criminal == choice - 1) { Console.WriteLine("Вы нашли преступника!"); Win = true; }
                else Console.WriteLine("Вы посадили не того человека. Итог: вас уволили...");
            }
            else Console.WriteLine("Вы выбрали несуществующего человека, вы проиграли");
            Next();
            GameStart = false;
        }
        private void Next()//Вспомогательный метод
        {
            Console.Write("\nНажмите любую кнопку чтобы продолжить");
            //Console.ReadLine();
            Console.ReadKey();
            Console.Clear();
        }
    }
}