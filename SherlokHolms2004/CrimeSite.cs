﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SherlokHolms2004
{
    internal class CrimeSite
    {
        Random rnd;
        // Массив с участниками преступления
        public People[] participants = new People[4];
        public string[] crimetypes = { "Ограбление", "Убийство" }; //Вариации преступления
        public string[] crimethemes = { "Казино", "Магазин", "Яхта", "Подземный переход", "Небоскреб", "Метро", "Аэропорт" };//Вариации места преступления
        public int crimetype, crimetheme;

        public CrimeSite()
        {
            rnd = new Random();
            GenCrimeSite();
        }

        private void GenCrimeSite() //Создание Криминальной ситуации
        {
            crimetype = rnd.Next(crimetypes.Length);
            crimetheme = rnd.Next(crimethemes.Length);
            People p;
            for (int i = 0; i < participants.Length; i++)
            {
                p = new People();
                Thread.Sleep(100);
                participants[i] = p;
            }
        }
    }
}
