﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SherlokHolms2004
{
    public class People //Класс для создание человека
    {
        Random rnd = new Random();

        public string name, sername, pol;
        public int gender, height, weight;
        public string[] hab = new string[3];

        private string[] habbits = new string[8] { "Левша", "Косоглазый", "Горбатый", "Одноглазый", "Накаченный", "Лысый", "В очках", "Хромой" };

        private string[] names_F = new string[30] { "Елизавета", "Амира", "Дарья", "Ева", "Владислава", "Елена", "Агата", "Милана", "Арина", "Ольга", "София", "Стефания", "Виктория", "Варвара", "Ясмина", "Полина", "Вера", "Анна", "Сафия", "Мария", "Ксения", "Екатерина", "Алёна", "Эмилия", "Есения", "Злата", "Ярослава", "Айлин", "Нелли", "Алиса" };
        private string[] names_M = new string[30] { "Артём", "Андрей", "Александр", "Денис", "Юрий", "Богдан", "Савелий", "Лука", "Леонид", "Тимофей", "Мирослав", "Владимир", "Фёдор", "Никита", "Максим", "Даниил", "Дмитрий", "Платон", "Руслан", "Григорий", "Степан", "Сергей", "Николай", "Кирилл", "Матвей", "Марк", "Константин", "Арсений", "Илья", "Павел" };

        private string[] sernames_F = new string[30] { "Иванова", "Смирнова", "Кузнецова", "Попова", "Васильева", "Петрова", "Соколова", "Михайлова", "Новикова", "Федорова", "Морозова", "Волкова", "Алексеева", "Лебедева", "Семенова", "Егорова", "Павлова", "Козлова", "Степанова", "Никитина", "Орлова", "Андреева", "Макарова", "Николаева", "Захарова", "Жукова", "Борисова", "Медведева", "Ершова", "Романова" };
        private string[] sernames_M = new string[30] { "Иванов", "Смирнов", "Кузнецов", "Попов", "Васильев", "Петров", "Соколов", "Михайлов", "Новиков", "Федоров", "Морозов", "Волков", "Алексеев", "Лебедев", "Семенов", "Егоров", "Павлов", "Козлов", "Степанов", "Никитин", "Орлов", "Андреев", "Макаров", "Николаев", "Захаров", "Жуков", "Борисов", "Медведев", "Ершов", "Романов" };

        public People()
        {
            Generate_Persone();
        }

        private void Generate_Persone()// Основной метод генерации
        {
            gender = rnd.Next(0, 2);// Генерация пола
            int i = 0;
            while (i < 3) // Генерация приметы
            {
                int num = rnd.Next(0, 8);
                if (!hab.Contains(habbits[num]))
                {
                    hab[i] = habbits[num];
                    i++;
                }

            }
            if (gender == 0)// Генерация ФИо
            {
                pol = "Ж";
                name = names_F[rnd.Next(0, 30)];
                sername = sernames_F[rnd.Next(0, 30)];
            }
            else
            {
                pol = "М";
                name = names_M[rnd.Next(0, 30)];
                sername = sernames_M[rnd.Next(0, 30)];
            }
            height = rnd.Next(165, 200);// Генерация Роста
            if (gender == 0)// Генерация веса
            {
                height -= 10;
                weight = height - 110 + -rnd.Next(0, 5);
            }
            else
            {
                weight = height - 100 + -rnd.Next(0, 5);
            }

        }

        public string Get_Persone()// Метод вывода информации о человеке
        {

            string persone = "ФИО: " + sername + " " + name + "\n" + "Пол: " + pol + "\n" + "Рост и Вес: " + height + " " + weight + "\n" + "Особые приметы: ";
            for (int i = 0; i < hab.Length; i++)
            {
                persone += hab[i] + ", ";
            }
            return persone;
        }

    }
}
